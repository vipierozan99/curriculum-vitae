# Victor Pierozan's CV

[![Netlify Status](https://api.netlify.com/api/v1/badges/8b420ff0-553e-432d-9dc8-ca7c94df6015/deploy-status)](https://app.netlify.com/sites/victorpierozan/deploys)

This is a digital version of my CV, made with **React**, **MDX**, and **TailwindCSS**.

