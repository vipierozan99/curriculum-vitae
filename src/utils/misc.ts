import { type ClassValue, clsx } from "clsx";
import { twMerge } from "tailwind-merge";

export const cn = (...inputs: ClassValue[]) => {
	return twMerge(clsx(inputs));
};

export function zip<T>(
	...arrays: Array<readonly T[]>
): Array<(T | undefined)[]> {
	const max_len = Math.max(...arrays.map((array) => array.length));
	const result: Array<(T | undefined)[]> = [];
	for (let i = 0; i < max_len; i++) {
		result.push(arrays.map((array) => array.at(i)));
	}
	return result;
}
