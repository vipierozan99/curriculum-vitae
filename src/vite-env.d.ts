/// <reference types="vite/client" />

declare module "*.mdx" {
	let MDXComponent: (props: MDXProps) => JSX.Element;
	export default MDXComponent;
}
