import Markdown from "react-markdown";

export function PersonalSection({ children }: { children: string }) {
	return (
		<Markdown
			components={{
				h1: (props) => <h1 {...props} className="text-4xl font-bold" />,
			}}
		>
			{children}
		</Markdown>
	);
}
