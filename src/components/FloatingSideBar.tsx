import GitLabLogo from "@/assets/images/gitlab.svg";

export function FloatingSideBar() {
	return (
		<div
			className={
				"fixed top-8 flex flex-col gap-2  w-[16rem] left-[calc(((50vw-10.5cm)/2)-8rem)] bg-gray-700 rounded-md p-4 text-white print:hidden"
			}
		>
			<h1 className="text-2xl font-bold">Welcome to my CV!</h1>

			<p className="text-sm">
				This is a digital version of my CV, made with React, MDX, and
				TailwindCSS. You can find the source code here:
			</p>
			<a
				href="https://gitlab.com/vipierozan99/curriculum-vitae"
				target="_blank"
				rel="noopener noreferrer"
				className="flex flex-row gap-2 items-center text-xl self-center bg-primary-500 p-1 pr-2 rounded-md"
			>
				<img src={GitLabLogo} alt="GitLab" className="w-6 h-6" />
				GitLab
			</a>

			<p className="text-sm">
				You can download the PDF version of my CV by printing this page with the
				browser's print function or by clicking this button:
			</p>

			<button
				className="bg-primary-500 font-bold px-4 py-2 rounded-md"
				type="button"
				onClick={() => window.print()}
			>
				Download PDF
			</button>
		</div>
	);
}
