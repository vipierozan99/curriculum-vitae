import type { HTMLAttributes } from "react";
import { cn } from "../utils/misc";

export function SectionHeader({
	company,
	title,
	from,
	to,
	className,
	...props
}: {
	company: string;
	title: string;
	from: string;
	to?: string;
} & HTMLAttributes<HTMLDivElement>) {
	return (
		<div {...props} className={cn("flex flex-col pb-1 pt-2", className)}>
			<div className="flex flex-row gap-2">
				<h1 className="flex-1 font-bold text-xl leading-[normal]">{title}</h1>
				<p className="self-end text-sm">
					{from} {to ? `→ ${to}` : ""}
				</p>
			</div>
			<p className="leading-none italic">{company}</p>
		</div>
	);
}
