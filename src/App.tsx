import type { MDXProps } from "mdx/types";
import type { HTMLAttributes } from "react";
import { FloatingSideBar } from "./components/FloatingSideBar";
import {
	AIPrompt,
	HeroContainer,
	LeftColumn,
	Page,
	ProfilePicture,
	RightColumn,
} from "./components/Page";
import * as LeftSections from "./components/content/LeftSections.mdx";
import Personal from "./components/content/Personal.mdx";
import * as RightSections from "./components/content/RightSections.mdx";
import { zip } from "./utils/misc";
export function App() {
	const [rightPage1, ...rightPages] = getPages(RightSections);
	const [leftPage1, ...leftPages] = getPages(LeftSections);

	return (
		<main className="bg-gray-600 flex flex-col p-4 gap-4 print:gap-0 print:p-0 leading-[normal]">
			<FloatingSideBar />

			<Page>
				<LeftColumn>
					<HeroContainer>
						<ProfilePicture />
						<Personal
							components={{
								...components,
								h1: (props: HTMLAttributes<HTMLHeadingElement>) => (
									<h1 {...props} className="text-4xl font-bold" />
								),
							}}
						/>
					</HeroContainer>
					{leftPage1({ components })}
					<AIPrompt />
				</LeftColumn>
				<RightColumn>{rightPage1({ components })}</RightColumn>
			</Page>

			{Array.from(zip(leftPages, rightPages)).map(([left, right], idx) => (
				// biome-ignore lint/suspicious/noArrayIndexKey: <explanation>
				<Page key={idx}>
					<LeftColumn>{left?.({ components })}</LeftColumn>
					<RightColumn>{right?.({ components })}</RightColumn>
				</Page>
			))}
		</main>
	);
}

export default App;

const components = {
	h1: (props: HTMLAttributes<HTMLHeadingElement>) => (
		<h1
			{...props}
			className="text-2xl font-bold text-primary-300 border-b-2 border-primary-300 mb-2 mt-2"
		/>
	),
	ul: (props: HTMLAttributes<HTMLUListElement>) => (
		<ul {...props} className="list-disc ml-4" />
	),
	a: (props: HTMLAttributes<HTMLAnchorElement>) => (
		<a
			{...props}
			className="underline"
			target="_blank"
			rel="noopener noreferrer"
		/>
	),
};

function getPages(module: any): ((props: MDXProps) => React.ReactNode)[] {
	return (
		Object.entries(module)
			.filter(([key, _value]) => key !== "default")
			.toSorted((a, b) => a[0].localeCompare(b[0]))
			// @ts-ignore
			.map(([_key, value]) => value.default)
	);
}
