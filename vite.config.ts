import recmaSection from "@frontline-hq/recma-sections";
import mdx from "@mdx-js/rollup";
import tailwindcss from "@tailwindcss/vite";
import react from "@vitejs/plugin-react-swc";
import { defineConfig } from "vite";
import tsconfigPaths from "vite-tsconfig-paths";

// https://vite.dev/config/
export default defineConfig({
	plugins: [
		tsconfigPaths(),
		{
			enforce: "pre",
			...mdx({
				jsxImportSource: "react",
				recmaPlugins: [[recmaSection, { getComment: getComment }]],
			}),
		},
		react(),
		tailwindcss(),
	],
});

function getComment(comment: string) {
	if (!comment) return undefined;
	return comment.trim().startsWith("p:") ? comment.trim().slice(2) : undefined;
}
